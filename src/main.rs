use std::env;
use std::fs::File;
use std::fs;
use std::io::{BufRead, BufReader};
use std::process::Command;
// for indicatif
use console::{style, Emoji};
use std::time::{Instant};
use indicatif::{HumanDuration, ProgressBar, ProgressStyle, ProgressState};
use std::process::Stdio;
use std::{fmt::Write};
use std::process;
#[cfg(windows)]
use winapi::um::winuser::{MessageBoxW, MB_ICONQUESTION, MB_YESNO};


#[cfg(target_os = "windows")]
fn is_command_installed(command: &str) -> bool {
    let status = Command::new("where")
        .arg(command)
        .status()
        .expect("failed to execute process");

    status.success()
}

#[cfg(not(target_os = "windows"))]
fn is_command_installed(command: &str) -> bool {
    let output = Command::new("which")
        .arg(command)
        .output()
        .expect("failed to execute process");

    output.status.success()
}

const GALLERYDL : &str = "gallery-dl";

fn main() {

    if cfg!(target_os = "windows") {
        #[cfg(target_os = "windows")]
        if !is_command_installed(GALLERYDL) {
            let uri = format!("ms-windows-store://pdp/?productid=9NBLGGH4NNS1");
    
            // Launch the Microsoft Store with the product page URI as the argument
            Command::new("powershell")
                .arg("start")
                .arg(uri)
                .output()
                .expect("Failed to open Microsoft Store");
        
        
            // Call the external function
            let message = "Please install and update this app installer package to get gallery-dl! and then install gellry-dl via python or winget";
            let caption = "Installing dependencies";
            unsafe {
                MessageBoxW(
                    std::ptr::null_mut(),
                    message.encode_utf16().chain(std::iter::once(0)).collect::<Vec<_>>().as_ptr(),
                    caption.encode_utf16().chain(std::iter::once(0)).collect::<Vec<_>>().as_ptr(),
                    0,
                );
            }

            process::exit(1);

            //Command::new("powershell")
            //.arg("winget install gallery-dl")
            //.output()
            //.expect("Failed to install gellry-dl");

            //Command::new("cmd")
            //    .arg("/C")
            //    .arg("setx")
            //    .arg("PATH")
            //    .arg("%PATH%")
            //    .output()
            //    .expect("Failed to reload environment variables");

            //        // Ask the user if the program can do something
            //let message = "Can the program do something?";
            //let caption = "Permission required";
            //let result = unsafe {
            //    MessageBoxW(
            //        std::ptr::null_mut(),
            //        message.encode_utf16().chain(Some(0)).collect::<Vec<u16>>().as_ptr(),
            //        caption.encode_utf16().chain(Some(0)).collect::<Vec<u16>>().as_ptr(),
            //        MB_ICONQUESTION | MB_YESNO,
            //    )
            //};

            //// Check the user's answer
            //match result {
            //    6 => {
            //        println!("User clicked Yes, doing something...");
            //        // Do something here
            //    }
            //    _ => {
            //        println!("User clicked No, exiting...");
            //        std::process::exit(0);
            //    }
            //}

        }
    } else if cfg!(target_os = "linux") {
        if !is_command_installed(GALLERYDL) {
            println!("{} is not installed.", GALLERYDL);
        
            // Ask if it can download gallery-dl
            println!("Do you want to install {} (y/n)", GALLERYDL);
            
        
            let mut input = String::new();
            std::io::stdin().read_line(&mut input).unwrap();
            let input = input.trim();
        
            if input == "y" {
                println!("Detected {} operating system", std::env::consts::OS);
                let package_manager = match true {
                    _ if is_command_installed("yay") => "yay",
                    _ if is_command_installed("emerge") => "emerge",
                    _ if is_command_installed("apt") => "apt",
                    _ => {
                        println!("No package manager detected that can install {}", GALLERYDL);
                        process::exit(1);
                    }
                };
        
                println!("Downloading {} using {}", GALLERYDL, package_manager);
        
                // Install gallery-dl using the appropriate package manager
                let mut command = match package_manager {
                    "yay" => {
                        let mut cmd = Command::new("sudo");
                        cmd.arg("yay").arg("-S").arg("--noconfirm").arg(GALLERYDL);
                        cmd
                    },
                    "emerge" => {
                        let mut cmd = Command::new("sudo");
                        cmd.arg("emerge").arg(GALLERYDL);
                        cmd
                    },
                    "apt" => {
                        let mut cmd = Command::new("sudo");
                        cmd.arg("apt").arg("install").arg(GALLERYDL);
                        cmd
                    },
                    _ => {
                        println!("Unsupported package manager: {}", package_manager);
                        process::exit(1);
                    }
                };
                
                let output = command.output().expect("failed to execute package manager command");   
                     
                if output.status.success() {
                    println!("{} has been installed!", GALLERYDL);
                } else {
                    println!("failed to install {}", GALLERYDL);
                    println!("Please install it manually");
                    println!("and make sure it's in $PATH");
                    process::exit(1);
                }
            } else {
                println!("Can't run without {}", GALLERYDL);
                process::exit(1);
            }
        }
    } else if cfg!(target_os = "freebsd") {
        if !is_command_installed(GALLERYDL) {
            println!("gallery-dl is not installed.");
     
            // Ask if it can download gallery-dl
            println!("Do you want to install gallery-dl using apt (y/n)");
            let mut input = String::new();
            std::io::stdin().read_line(&mut input).unwrap();
            let input = input.trim();

            if input == "y" {
                // Install gallery-dl using apt-get
                let output = Command::new("sudo")
                    .arg("apt")
                    .arg("install")
                    .arg("gallery-dl")
                    .output()
                    .expect("failed to execute apt-get command");

                if output.status.success() {
                    println!("gallery-dl has been installed!");
                } else {
                    println!("failed to install gallery-dl");
                    println!("Please install it manually");
                    println!("and make sure its in $PATH");
                    process::exit(1);
                }
            } else {
                println!("Can't run without gallery-dl");
                process::exit(1);
            }
            println!("gallery-dl is not installed on FreeBSD");
        } 
    } else {
        println!("cant automatically install dependencies in this operating system");
        println!("Please install gallery-dl manually");
    }

    static LOOKING_GLASS: Emoji<'_, '_> = Emoji("🔍  ", "");
    static TRUCK: Emoji<'_, '_> = Emoji("🚚  ", "");
    static SPARKLE: Emoji<'_, '_> = Emoji("✨ ", ":-)");
    let started = Instant::now();

    let args: Vec<String> = env::args().collect();

    if args.len() < 2 {
        eprintln!("Usage: ./program path-to-list-file [--videos | -v] [--pictures | -p] [--skip-progress | -s]");
        eprintln!("");
        eprintln!("[--skip-progress | -s] options skips getting list of files being downloaded");
        eprintln!("this removed the ability to see functioning progress bar");
        eprintln!("but will speed up the process of download");
        eprintln!("");
        eprintln!("list files should include all the links you wish to download, one link per line");
        eprintln!("downloaded files will be download next to binary");
        return;
    }

    //opens the file for downloading 
    let file = match File::open(&args[1]) {
        Ok(file) => file,
        Err(e) => {
            eprintln!("Error opening file: {}", e);
            return;
        }
    };

    //opens the file for counting
    let file_path = &args[1];
    let contents = fs::read_to_string(file_path)
        .expect("failed to read file");

    let mut extension_filter = "('mp4', 'm4v', 'webm', 'avi' ,'jpg', 'png', 'jpe', 'zip' ,'gif')";
    let mut skip_progress = false;

    for i in 2..args.len() {
        match args[i].as_str() {
            "-v" | "--videos" => extension_filter = "('mp4', 'm4v', 'webm', 'avi')",
            "-p" | "--pictures" => extension_filter = "('jpg', 'png', 'jpe', 'gif')",
            "-s" | "--skip-progress" => skip_progress = true,
            _ => {println!("Unknown argument: {}", args[i]); process::exit(1)},
        }
    }

    //urls for downloading
    let urls: Vec<String> = BufReader::new(file)
        .lines()
        .map(|line| line.unwrap())
        .collect();

    //all files that will be downloaded
    let mut all_files = 0;

    
    if skip_progress == false {
        println!(
            "{} {}Getting list of files...",
            style("[1/2]").bold().dim(),
            LOOKING_GLASS
        );

        for url in contents.lines() {
            let output = Command::new("gallery-dl")
                            .arg("--filter")
                            .arg(format!("extension in {}", extension_filter))
                            .arg("--no-download")
                            .arg(url)
                            .output()
                            .expect("failed to execute gallery-dl command");

            let stdout = String::from_utf8_lossy(&output.stdout);
            let num_files = stdout.lines().count();
            all_files += num_files;
            //println!("{}: Number of files: {}", url, num_files);
        }
    } else {
        println!(
            "{} {}Skipping getting list of files...",
            style("[1/2]").bold().dim(),
            LOOKING_GLASS
        );
    }
    println!(
        "{} {}Startind download of {} files...",
        style("[2/2]").bold().dim(),
        TRUCK,
        all_files
    );

    //progress bar
    let all_files_u64 = all_files.to_string().parse::<u64>().unwrap();
    let pb = ProgressBar::new(all_files_u64);

    for url in urls {
        let mut output = Command::new("gallery-dl")
            .arg("--filter")
            .arg(format!("extension in {}", extension_filter))
            .arg(url)
            .stdout(Stdio::piped())
            .spawn()
            /* .output() */
            .expect("failed to execute process");

        let stdout = output.stdout.take().unwrap();
        let reader = BufReader::new(stdout);
        

        for line_result in reader.lines() {
            if let Ok(_line) = line_result {
                //println!("{}: {}", line_count, line);
                pb.set_style(ProgressStyle::with_template("{spinner:.green} [{elapsed_precise}] [{wide_bar:.cyan/blue}] {pos}/{len} {percent}% ")
                .unwrap()
                .with_key("eta", |state: &ProgressState, w: &mut dyn Write| write!(w, "{:.1}s", state.eta().as_secs_f64()).unwrap())
                .progress_chars("#>-"));
                pb.inc(1);
            }
        }

        //println!("{}", String::from_utf8_lossy(&output.stdout));
    }
    println!("{} Done in {}", SPARKLE, HumanDuration(started.elapsed()));
}